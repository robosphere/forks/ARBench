import json
import os


class FileSystemRepository:
    def readJSON(path: str):
        return json.loads((open(path)).read())

    def writeFile(data, filePath, fileName):
        file_to_open = filePath + fileName

        f = open(file_to_open, "w", encoding="utf-8", errors="ignore")
        f.write(data)
        f.close()

    def readFile(path: str):
        return open(path).read()

    def readFilesTypeFolder(pathFolder: str, fileType=".json"):
        filesJson = list(
            filter(
                lambda x: x[-fileType.__len__() :] == fileType, os.listdir(pathFolder)
            )
        )
        return filesJson
