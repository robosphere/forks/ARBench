import argparse
from usecases.stability_check_usecase import StabilityCheckUseCase

# python3 main.py --aspPath /Users/idontsudo/Desktop/asp-example/


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--aspPath", help="asp folder generation path")
    args = parser.parse_args()
    StabilityCheckUseCase().call(args.aspPath)


main()
