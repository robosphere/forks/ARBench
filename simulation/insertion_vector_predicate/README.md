# Start dev
create env.json

```json
{
    "cadDoc":"CAD_DOC_PATH_REPLACE",
    "sequences":"SEQUENCES_PATH_REPLACE",
    "aspDir":"ASP_DIR_REPLACE"
}
```
# Command generation assets
freecad generate.py

# Command generation insertion vectors

Загрузка субмодуля git  
```
git submodule update --init 
```

Создание и активация виртуального окружения
```
conda env create -f assembly/environment.yml 
 
conda activate assembly  
```

Запуск программы
```
python3 main.py
```