from distutils.dir_util import copy_tree
from src.model.enum import Enum

class Assembly:
    def generateSubAssembly(self, assembly: list[str]):
        asm = {}
        inc = 0
        for el in assembly:
            asm[str("asm" + str(inc))] = {
                "part": el,
                "assembly": assembly[0:inc],
            }
            inc += 1
        return asm
    def copy(self,generationFolder,format,outPath  ):
        copy_tree(generationFolder + format, outPath + Enum.folderPath)