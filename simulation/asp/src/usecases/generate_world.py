import os
from helper.fs import FS


class SdfGenerateWorldUseCase:
    def call(assembly: str) -> str:
        world = FS.readFile(
            os.path.dirname(os.path.realpath(__file__)) + "/../../mocks/sdf/world.sdf"
        )
        beginWorld = world[0 : world.find("</world") - 1]
        endWorld = world[world.find("</world") - 1 : world.__len__()]

        return beginWorld + assembly + endWorld


class GeometryValidateUseCase:
    def call(geometry) -> str:
        return
