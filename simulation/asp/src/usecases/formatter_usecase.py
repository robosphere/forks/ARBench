from helper.xmlformatter import Formatter
from src.model.enum import Enum
from helper.fs import FS
 
class FormatterUseCase:
    def call(outPath: str,   format: str):
        formatter = Formatter(
            indent="1", indent_char="\t", encoding_output="ISO-8859-1", preserve=["literal"])

        files = FS.readFilesTypeFolder(
            outPath + Enum.folderPath, fileType=format)
        for el in files:
            FS.writeFile(data=str(formatter.format_file(outPath + Enum.folderPath + el),
                         'utf-8'), filePath=outPath + Enum.folderPath, fileName=el)
