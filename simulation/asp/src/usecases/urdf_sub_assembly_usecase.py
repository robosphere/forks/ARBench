from helper.fs import FS
from src.model.enum import Enum
from src.model.asm import Assembly
from src.model.sdf_geometry import GeometryModel
import json
import re


URDF_FILE_FORMAT = ".urdf"
URDF_GENERATOR_FILE = "urdf-generation" + ".json"


class UrdfSubAssemblyUseCase(Assembly):
    def call(
        self,
        geometryModels: list[GeometryModel],
        assembly: list[str],
        outPath: str,
        generationFolder: str,
        world: bool,
    ):
        dirPath = generationFolder + Enum.folderPath
        asm = {}
        for el in geometryModels:
            asm[el.name] = el.toUrdf()
        FS.writeFile(
            data=json.dumps(asm, indent=4),
            fileName=URDF_GENERATOR_FILE,
            filePath=dirPath,
        )
