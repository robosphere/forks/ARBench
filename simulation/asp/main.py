import argparse
import shutil
from src.model.enum import Enum
from helper.fs import FS
from src.usecases.urdf_sub_assembly_usecase import UrdfSubAssemblyUseCase
from src.model.sdf_geometry import GeometryModel
from src.usecases.sdf_sub_assembly_usecase import SdfSubAssemblyUseCase

import os
from pathlib import Path


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--generationFolder", help="FreeCad generation folder")
    parser.add_argument("--outPath", help="save SDF path")
    parser.add_argument("--world", help="adding sdf world")
    parser.add_argument("--format", help="urdf,sdf,mujoco")
    args = parser.parse_args()

    if args.generationFolder == None or args.outPath == None:
        parser.print_help()
    outPath = args.outPath
    geometryFiles = FS.readFilesTypeFolder(args.generationFolder + "/assets/")
    assemblyStructure = FS.readJSON(args.generationFolder + "/step-structure.json")

    geometryModels: list[GeometryModel] = []
    for el in geometryFiles:
        geometryModels.append(
            GeometryModel.from_dict(
                FS.readJSON(args.generationFolder + "/assets/" + el)
            )
        )
    if os.path.exists(outPath + Enum.folderPath):
        shutil.rmtree(outPath + Enum.folderPath)
    Path(outPath + Enum.folderPath).mkdir(parents=True, exist_ok=True)

    if args.format == "sdf":
        SdfSubAssemblyUseCase().call(
            geometryModels=geometryModels,
            assembly=assemblyStructure,
            world=args.world,
            generationFolder=args.generationFolder,
            outPath=args.outPath,
        )
    if args.format == "urdf":
        UrdfSubAssemblyUseCase().call(
            geometryModels=geometryModels,
            assembly=assemblyStructure,
            world=args.world,
            generationFolder=args.generationFolder,
            outPath=args.outPath,
        )
