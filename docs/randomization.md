# Рандомизация

### При обучении робота (Илья Ураев)

- Гравитация
- Положение камеры
- Конфигурацию робота (положение джоинтов так называется)
- Положение объекта или точки куда надо дотянутся
- Текстуру поверхности размещения
- Ну и я думаю чтобы с robot_builder смотрелось, то можно рандомизировать число степеней свободы робота.
- Можно рандомизировать позиции спавна робота

### При создании датасета (Александр Шушпанов)

- Зона локации (спавна) активных объектов
- Позиция камеры: радиус сферы размещения и наклон, центр этой сферы
- Источники света: количество, тип, локализация в пространстве, цветность, сила света
- Свойства материала по отражению света: зеркальность, шероховатость, металлизация, цвет

  Гиперпараметры.
- количество серий (спавна активных объектов)
- количество позиций камеры на одну серию
- количество сдвигов камеры на 1 позу

### Общий список параметров рандомизации

- Положение искомого(активного) объекта(-ов) в рабочей зоне
- Позиция камеры: радиус сферы размещения и наклон, центр этой сферы
- Текстуры повехностей объектов и/или свойства материала по отражению света: зеркальность, шероховатость, металлизация, цвет
- Источники света: количество, тип, локализация в пространстве, цветность, сила света
- Конфигурация робота (положение джоинтов), число степеней свободы и его начальное расположение
- Гравитация

## Web-сервис для генерации датасетов

Для реализации пользовательского интерфейса web-сервиса нами была разработана схема описания параметров рандомизации. Её использование позволяет изменять конфигурацию параметров в зависимости от задачи. Предполагается в дальнейшем использовать модуль ввода параметров в том числе и в задачах обучения с подкреплением.  

Пример такой схемы:
```
ENUM T = "ObjectDetection","PoseEstimation"
ENUM C = "","BOX","SPHERE","CAPSULE","CYLINDER","CONE","CONVEX_HULL","MESH","COMPOUND"
ENUM L = "POINT","SUN"
ENUM F = "JPEG","PNG"

MODELS = {
  "id": ${ID:number:1},
  "name": ${NAME:string:""},
  "model": ${MODEL:string:"models/1.fbx"}
}
OBJECTS_SCENE = {
  "name": ${NAME:string:""},
  "collision_shape": ${enum:C:"BOX"},
  "loc_xyz": [${LOC_XYZ_1:number:0}, ${LOC_XYZ_2:number:0}, ${LOC_XYZ_3:number:0}],
  "rot_euler": [${ROT_EULER_1:number:0}, ${ROT_EULER_2:number:0}, ${ROT_EULER_3:number:0}],
  "material_randomization": {
    "specular": [${SPECULAR_1:number:0}, ${SPECULAR_2:number:1}],
    "roughness": [${ROUGHNESS_1:number:0}, ${ROUGHNESS_2:number:1}],
    "metallic": [${METALLIC_1:number:0}, ${METALLIC_2:number:1}],
    "base_color": [
          [
            ${BASE_COLOR_1:number:0},
            ${BASE_COLOR_2:number:0},
            ${BASE_COLOR_3:number:0},
            ${BASE_COLOR_4:number:1}
          ],
          [
            ${BASE_COLOR_5:number:1},
            ${BASE_COLOR_6:number:1},
            ${BASE_COLOR_7:number:1},
            ${BASE_COLOR_8:number:1}
          ]
        ]
  }
}
LIGHTS = {
  "id": ${ID:number:1},
  "type": ${enum:L:"POINT"},
  "loc_xyz": [${LOC_XYZ_1:number:5}, ${LOC_XYZ_2:number:5}, ${LOC_XYZ_3:number:5}],
  "rot_euler": [${ROT_EULER_1:number:-0.06}, ${ROT_EULER_2:number:0.61}, ${ROT_EULER_3:number:-0.19}],
  "color_range_low": [${COLOR_RANGE_LOW_1:number:0.5}, ${COLOR_RANGE_LOW_2:number:0.5}, ${COLOR_RANGE_LOW_3:number:0.5}],
  "color_range_high":[${COLOR_RANGE_HIGH_1:number:1}, ${COLOR_RANGE_HIGH_2:number:1}, ${COLOR_RANGE_HIGH_3:number:1}],
  "energy_range":[${ENERGY_RANGE_1:number:400},${ENERGY_RANGE_2:number:900}]
}

{
  "typedataset": ${enum:T:"ObjectDetection"},
  "dataset_path": ${DATASET_PATH:string},
  "models":${ARRAY:MODELS:[]},
  "models_randomization":{
    "loc_range_low":  [${LOC_RANGE_LOW_1:number:-1}, ${LOC_RANGE_LOW_2:number:-1}, ${LOC_RANGE_LOW_3:number:0}],
    "loc_range_high": [${LOC_RANGE_HIGH_1:number:1}, ${LOC_RANGE_HIGH_2:number:1}, ${LOC_RANGE_HIGH_3:number:2}]
  },
  "scene":{
    "objects": ${ARRAY:OBJECTS_SCENE:[]},
    "lights": ${ARRAY:LIGHTS:[]},
  },
  "camera_position":{
    "center_shell": [${CENTER_SHELL_1:number:0}, ${CENTER_SHELL_2:number:0}, ${CENTER_SHELL_3:number:0}],
    "radius_range": [${RADIUS_RANGE_1:number:0.4}, ${RADIUS_RANGE_2:number:1.4}],
    "elevation_range": [${ELEVATION_RANGE_1:number:10}, ${ELEVATION_RANGE_2:number:90}]
  },
  "generation":{
    "n_cam_pose": ${N_CAM_POSE:number:5},
    "n_sample_on_pose": ${N_SAMPLE_ON_POSE:number:3},
    "n_series": ${N_SERIES:number:100},
    "image_format": ${enum:F:"jpg"},
    "image_size_wh": [${IMAGE_SIZE_WH_1:number:640}, ${IMAGE_SIZE_WH_2:number:480}]
  }
}
```

Вначале описываются перечисления - ENUM, которым присваиваются имена и список возможных значений. Затем описываются составные именованные объекты, а затем основной блок описания параметров. Этот блок представляет из себя JSON-словарь параметров, который будет подготовлен на выходе модуля ввода параметров. Каждый ключ этого словаря дополняется мини-схемой описания вводимого значения.  
Формат:
```
${<имя_переменной>:<тип>:<значение_по_умолчанию>}
```
либо массив объектов
```
${ARRAY:<имя_объекта>:[]}
```

В нашем алгоритме формирования датасета для задач компьютерного зрения (ObjectDetection, PoseEstimation) выбран формат [BOP: Benchmark for 6D Object Pose Estimation](https://bop.felk.cvut.cz/home/), в его [BOP-Classic](https://bop.felk.cvut.cz/datasets/) версии.  
Он содержит в своей аннотации все необходимые данные (ground truth) для обучения нейросетевых моделей поиску объектов на изображении, а также распознавания поз искомых объектов.