# coding: utf-8
# Copyright (C) 2023 Ilia Kurochkin <brothermechanic@yandex.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
'''
DESCRIPTION.
Run cmd program with args and kwargs.
Get return via cmd output.
'''
__version__ = '0.1'

import subprocess


def cmd_proc(*args, **kwargs):
    command = list(args)
    for akey, aval in kwargs.items():
        command.append(f'--{akey}')
        command.append(str(aval))

    return subprocess.run(command,
                          check=True,
                          stdout=subprocess.PIPE,
                          encoding='utf-8').stdout
